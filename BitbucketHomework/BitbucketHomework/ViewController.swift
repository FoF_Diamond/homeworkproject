//
//  ViewController.swift
//  BitbucketHomework
//
//  Created by Victoria on 14.11.21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textLabel.isHidden = true
        
    }

    @IBAction func Action(_ sender: Any) {
        if textField.text != "" {
            textLabel.text = textField.text
            textLabel.isHidden = false
        }
    }
    @IBAction func nextPage(_ sender: Any) {
        let secondVc = SecondVC(nibName: String(describing: SecondVC.self), bundle: nil)
        navigationController?.pushViewController(secondVc, animated: true)
    }
}
